#! /bin/bash


#######################
# root user credentials

[ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"


##########################################################################
# check /etc/openvpn dir if surfshark files exist and download them if not

#if [[ ( ! -f /etc/openvpn/*ovpn ) || ( 'sudo ls /etc/openvpn/*surfshark*ovpn | wc -l' < 250 ) ]] ## keeping as a reference for future improvements

FOUND=

for FILE in /etc/openvpn/*surfshark*.ovpn
do
	if [ -f "$FILE" ]
	then
		FOUND="$FILE"
		break
	fi
done

if [ -f "$FOUND" ];
then
	echo -e "\e[1;36m Files are already downloaded \e[0m"
else
	echo -e "\e[1;36m Downloading Surfshark ovpn files \e[0m"
    	cd /etc/openvpn
    	sudo wget https://my.surfshark.com/vpn/api/v1/server/configurations
    	sudo unzip configurations
    	sudo rm configurations
    	cd -
fi

###########################################################
# provide SurfShark VPN credentials to store them in a file

pass=/etc/openvpn/client/surfshark.pass

if [ ! -f $pass ] ; 
then
    echo "Following credentials can be found in https://my.surfshark.com/vpn/manual-setup/main"
        echo -n 'Provide your SurfShark username: '
        read -s
        echo "$REPLY" > $pass
        echo
        echo -n 'Provide your SurfShark password: '
        read -s
        echo "$REPLY" >> $pass
	echo
else
#    echo -e "\e[1;36m Surfshark credentials ar allready stored do you want to update them?\e[0m"
    while true
do
 read -r -p "Surfshark credentials ar allready stored do you want to update them? [y/N] " input

 case $input in
     [yY][eE][sS]|[yY])
        echo "Yes"
        echo -n 'Provide your SurfShark username: '
        read -s
        echo "$REPLY" > $pass
        echo
        echo -n 'Provide your SurfShark password: '
        read -s
        echo "$REPLY" >> $pass
	echo
 $prompt
 break
 ;;
     [nN][oO]|[nN]*)
 echo "No"
 break
        ;;
     *)
 echo "Invalid input..."
 ;;
 esac
done
fi


########################
# select server location

list="$(cd /etc/openvpn/ && ls *tcp.ovpn)"
num="$(ls /etc/openvpn/*tcp.ovpn|wc -l)"
PS3='Choose the type of the connention from the list: '
    select server in $list ;
    do 
        if [ $REPLY -lt $num ] && [ $REPLY -gt 0 ];
        then
    echo -e "You selected \e[4;34m ${server} \e[0m"
    break;
        else
    echo "Number you have entered is incorect. Try again or press Ctrl-C to exit"
        fi
done


########################################################
# check if transmission container is running and stop it
#
# if [ 'docker ps | grep transmission' ];
# then
# 	  {sudo docker stop transmission} &> /dev/null
# fi
#
#
##########################################################
# check if the ovpn service is running and stop if running

if [ 'sudo systemctl status openvpn-client@*.service | grep active' ]; 
then
    sudo systemctl stop openvpn-client@*.service
fi


#################################################
# create the conf file for the choosen VPN server

conf=/etc/openvpn/surfshark.conf
client=/etc/openvpn/client/surfshark.conf
sudo cp /etc/openvpn/$server $conf
sed -i 's/auth-user-pass/auth-user-pass \/etc\/openvpn\/client\/surfshark.pass/' $conf
sudo mv $conf $client


##############################
# start openvpn-client service
    
    sudo systemctl enable openvpn-client@surfshark.service
    sudo systemctl start openvpn-client@surfshark.service
    echo


##########################################################
# check if the ovpn service is running and stop if running

if [ 'sudo systemctl status openvpn-client@*.service | grep running' ]; 
then
    echo -e "\e[1;36m $server has been conected \e[0m"
else
    echo -e "\e[1;31m opnevpn connection didn't connect please make sure that you have correct credentials and run the script again \e[0m"
fi

exit
